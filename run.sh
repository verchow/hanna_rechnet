#!/bin/sh

VENV=./venv.hanna_rechnet

python3 -m venv $VENV

$VENV/bin/pip install -e .

source $VENV/bin/activate

hanna_rechnet
