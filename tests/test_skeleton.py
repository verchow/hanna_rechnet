# -*- coding: utf-8 -*-

import pytest
from hanna_rechnet.skeleton import fib

__author__ = "Thomas Verchow"
__copyright__ = "Thomas Verchow"
__license__ = "mit"


def test_fib():
    assert fib(1) == 1
    assert fib(2) == 1
    assert fib(7) == 13
    with pytest.raises(AssertionError):
        fib(-10)
