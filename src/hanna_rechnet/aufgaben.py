# -*- coding: utf-8 -*-

from random import randint, choice


def bis_zum_naechsten_zehner():
    x = randint(1, 8)
    y = randint(1, 9)

    aufgabe = f"Von { 10 * x + y } zum nächsten Zehner?"
    ergebnis = 10 - y

    return (aufgabe, ergebnis, 3)


def minus_einstellig_ueber_zehnergrenze_hinweg_unter_100():
    x = randint(1, 9)
    y = randint(x, 9)
    z = randint(1, 9)

    aufgabe = f"{ z }{ x } - { y } = ?"
    ergebnis = (10 * z + x) - y

    return (aufgabe, ergebnis, 10)


def minus_einstellig_unter_100():
    aufgabe, ergebnis = _minus(range(10,100), range(1,10))
    return aufgabe, ergebnis, 10


def _minus(minuend: list, subtrahend: list):
    x = choice(minuend)
    y = choice(subtrahend)

    aufgabe = f"{ x } - { y } = ?"
    ergebnis = x - y

    return aufgabe, ergebnis


def plus_10_bis_19_unter_100():
    aufgabe, ergebnis = _plus(range(1, 80), range(10, 20))
    return aufgabe, ergebnis, 10


def _plus(summand_1: list, summand_2: list):
    x = choice(summand_1)
    y = choice(summand_2)

    aufgabe = f"{ x } + { y } = ?"
    ergebnis = x + y

    return aufgabe, ergebnis
