#!/usr/bin/python3

import subprocess
from os import system
from sys import exit
from math import log10
from time import sleep
from random import choice, shuffle, seed
from readchar import readkey
from datetime import datetime
from inspect import getmembers, isfunction

import logging

logging.basicConfig(level=logging.WARNING)

from . import aufgaben

seed()

videos_filename = "videos.txt"
anzahl_aufgaben = 10

videos = []


def stelle_aufgabe(aufgabe, ergebnis):
    logging.debug(f"Aufgabe: {aufgabe}")
    logging.debug(f"Ergebnis: {ergebnis}")

    print(str(aufgabe))

    eingabe = []
    for i in range(0, 1 + int(log10(ergebnis))):
        eingabe.append(readkey())
        if eingabe[-1] == "x" or eingabe[-1] == "q":
            print("ENDE")
            exit(0)

    logging.debug(f"Eingabe: {str(eingabe)}")

    wert = 0
    potenz = 0
    try:
        while eingabe:
            wert += int(eingabe.pop()) * 10 ** potenz
            potenz += 1
    except:
        logging.warning("Kann Eingabe nicht in Wert wandeln.")
        wert = None

    logging.debug(f"Eingabewert: {wert}")

    return wert == ergebnis


def play_video():
    global videos
    if len(videos) < 1:
        with open(videos_filename, "r") as fh:
            videos = fh.readlines()
        shuffle(videos)
        logging.info(f"{len(videos)} videos geladen")

    logging.debug(f"noch {len(videos)} videos vorhanden")
    subprocess.run(["mpv", "--fs", videos.pop()])


def frage_weiter():
    print("Taste drücken, wenn es weiter gehen kann. Oder 'q' fuer ENDE.")
    k = readkey()
    if k == "q":
        print("ENDE")
        exit(0)


def main():
    relative_dauer = []
    mittel_relative_dauer = 1

    aufgaben_vorhanden = []
    for i in getmembers(aufgaben, isfunction):
        if i[0][0] != "_":  # remove _internal functions
            aufgaben_vorhanden.append(i)

    while True:
      #  system("clear")

        aufgabe_name, aufgabe_function = choice(aufgaben_vorhanden)
        aufgabe, ergebnis, zeitvorgabe = aufgabe_function()
        logging.info(f"{aufgabe_name} mit Zeitvorgabe {zeitvorgabe} Sekunden")

        richtige = f"Richtige: {len(relative_dauer)}"
        zeit = f"Zeit: {mittel_relative_dauer:.1f}"
        if mittel_relative_dauer <= 0.8:
            emoji = f"\U0001f603" # :-D
        elif mittel_relative_dauer <= 1:
            emoji = f"\U0001f642" # :-)
        elif mittel_relative_dauer <= 1.2:
            emoji = f"\U0001f610" # :-|
        else: # > 1.2
            emoji = f"\U0001f614" # :-(

        print(f"{richtige} {zeit} {emoji}\n")
        start = datetime.now()
        e = stelle_aufgabe(aufgabe, ergebnis)
        ende = datetime.now()

        if e:
            print("RICHTIG!")
            relative_dauer.insert(0, (ende - start).total_seconds() / zeitvorgabe)
            relative_dauer = relative_dauer[0:anzahl_aufgaben]
            mittel_relative_dauer = sum(relative_dauer)/len(relative_dauer)
            sleep(1)
        else:
            print("FALSCH!\n")
            print(f"Richtig wäre {ergebnis}.")
            relative_dauer = []
            mittel_relative_dauer = 1
            sleep(2)

        logging.debug(f"Dauer: {str(relative_dauer)}")

        logging.info(f"{len(relative_dauer)} Richtige im {mittel_relative_dauer:.2f}-fachen der Zeit.")

        if len(relative_dauer) >= anzahl_aufgaben and mittel_relative_dauer <= 1:
            print(
                f"\n\nJUHU! Für die letzten {anzahl_aufgaben} Aufgaben hast du {mittel_relative_dauer:.2f}-fache der Zeit gebraucht!\n"
            )
            play_video()
            relative_dauer = []
            frage_weiter()


if __name__ == "__main__":
    main()
